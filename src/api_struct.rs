use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Listenbrainz {
    pub listen_type: String,
    pub payload: Vec<ListenData>
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ListenData {
    pub listened_at: u64,
    pub track_metadata: TrackMetadata,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct TrackMetadata {
    pub artist_name: String,
    pub track_name: String,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct UserToken {
    pub token: String,
}

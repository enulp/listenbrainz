use crate::api_struct::*;
use serde_json;

pub fn submit_listens(api_address: String, user: &UserToken, to_submit: &Listenbrainz) {
    println!("submiting_listens");
    let bearer = format!("Bearer {}", user.token);
    let address = format!("{}/1/submit-listens", api_address);
    println!("address: {}", address);
    let resp = ureq::post(&address)
        .set("Content-type", "application/json")
        .set("Authorization", &bearer)
        .send_json(serde_json::json!(to_submit));
    println!("submit_listens: {:#?}", resp);
}
